﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDetector : MonoBehaviour
{
    [SerializeField] bool isDetecting = false;
    [SerializeField] bool isPlayerClose = false;
    [SerializeField] float stayTime = 3f;
    GameController gameController;
    PlayerController playerController;
    UIController uiController;
    float count = 0;
    readonly string PLAYER = "Player";

    void Start()
    {
        gameController = GameController.GetController();
        playerController = gameController.GetPlayer();
        uiController = gameController.GetUIController();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag(PLAYER) && isDetecting)
        {
            isPlayerClose = true;
            count = 0;
            uiController.StayCountImageShowOn(true);
        }
    }
    private void OnTriggerStay(Collider other)
    {

        if (other.transform.CompareTag(PLAYER) && isPlayerClose)
        {
            if (count > stayTime)
            {
                KickLadder();
            }
            else
            {
                count += Time.deltaTime;
                // update UI
                uiController.StayTimeCounter(count , stayTime);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.transform.CompareTag(PLAYER))
        {
            isPlayerClose = false;
            uiController.StayCountImageShowOn(false);
        }
    }
    void KickLadder()
    {
        Debug.Log("player kick ladder");
        transform.GetComponentInParent<LadderParty>().Die();
        transform.GetComponent<SphereCollider>().enabled = false;
        playerController.InitiateKick(transform);
        isPlayerClose = false;
        uiController.StayCountImageShowOn(false);
    }
    public void StartDetecting()
    {
        isDetecting = true;
    }
}
