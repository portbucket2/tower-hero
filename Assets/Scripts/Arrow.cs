﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    [SerializeField] Vector3 targetLocation;
    [SerializeField] private double speed = 1, height = 5, decayStart = .75, decayAmount = .25;
    [SerializeField] private bool jetPackOn = false;
    [SerializeField] private ParticleSystem trail;
    [SerializeField] private bool isCannonBall = false;
    [SerializeField] private ParticleSystem hitParticle;

    private JetPackPath path;
    GameController gameController;
    Transform player;
    SphereCollider coll;
    readonly string ENEMY = "Enemy";
    readonly string RAM = "Ram";
    readonly string LADDER = "Ladder";
    readonly string CATAPULT = "Catapult";
    readonly string PLAYER = "Player";
    private void Start()
    {
        gameController = GameController.GetController();
        coll = GetComponent<SphereCollider>();
    }


    private void FixedUpdate()
    {
        MoveWithJetPack();        
    }

    private void Update()
    {
        //GetJetPackOn();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag(ENEMY))
        {
            Debug.Log("Arrow hit enemy");
            other.GetComponent<EnemyControllerRange>().TakeDamege();
        }
        else if (other.transform.CompareTag(RAM))
        {
            Debug.Log("Arrow hit ram");
            other.GetComponent<BattaringRam>().TakeDamege();
        }
        else if (other.transform.CompareTag(LADDER))
        {
            Debug.Log("Arrow hit ladder");
            other.GetComponent<LadderParty>().TakeDamege();
        }
        else if (other.transform.CompareTag(CATAPULT))
        {
            Debug.Log("Arrow hit catapult");
            other.GetComponent<Catapult>().TakeDamege();
        }
        else if (other.transform.CompareTag(PLAYER))
        {
            Debug.Log("Arrow hit player");
            other.GetComponent<PlayerController>().TakeDamege();
        }
    }

    private void MoveWithJetPack()
    {
        if (jetPackOn)
        {
            transform.position = path.GetPosition();
            if (path.IsFinished())
            {
                transform.position = path.GetEndPosition();
                jetPackOn = false;
                ReachedDestination();
            }
            transform.LookAt(targetLocation);
        }
    }
    public void SetArrow(Vector3 targe, float _speed, float _height) {
        targetLocation = targe;
        speed = _speed;
        height = _height;
    }

    public void ShootArrow()
    {
        InitializeJetPackSequence();
    }

    private void InitializeJetPackSequence()
    {        
        Vector3 startPosition = transform.position;
        Vector3 endPosition = targetLocation;
        path = new JetPackPath(startPosition, endPosition, speed, height, decayStart, decayAmount);
        jetPackOn = true;
        trail.gameObject.SetActive(true);
        trail.Play();
    }
    void ReachedDestination()
    {
        ArrowRandomAngle();
        CollidersOff();
    }
    void ArrowRandomAngle() {
        //float angle = Vector3.Angle((gameController.transform.position - transform.position).normalized, transform.forward);
        //Debug.LogError("Arrow Angle: " + angle);
        transform.LookAt(gameController.transform.position);
        transform.eulerAngles = new Vector3(Random.Range(45f, 85f), transform.eulerAngles.y, transform.eulerAngles.z);
    }
    void CollidersOff()
    {
        hitParticle.Play();
        coll.enabled = false;
        if (isCannonBall)
        {
            transform.GetChild(0).GetComponent<CannonBallRoller>().ActivateCannonBall();
        }
        else
        {
            Destroy(gameObject, 3f);
        }
    }
}

public class JetPackPath
{
    private Vector3 startPosition, endPosition, distance;
    private double height, width;
    private double position, speed;
    private double decayAmount, decayStart, decayWindow;

    public JetPackPath(Vector3 startPosition, Vector3 endPosition, double speed, double height, double decayStart, double decayAmount)
    {
        this.startPosition = startPosition;
        this.endPosition = endPosition;
        this.speed = speed;
        this.height = height;
        this.decayStart = decayStart;
        this.decayAmount = decayAmount;
        distance = endPosition - startPosition;
        width = .5;
        decayWindow = 1 - decayStart;
    }

    public Vector3 GetPosition()
    {
        position += GetSpeed() * Time.fixedDeltaTime;
        return startPosition + new Vector3((float)GetXPosition(), (float)GetYPosition(), (float)GetZPosition());
    }

    public double GetSpeed()
    {
        if (position >= decayStart)
        {
            double decay = position - decayWindow;
            double proportion = decay / decayWindow;
            return speed - (proportion * speed * decayAmount);
        }
        return speed;
    }

    public Vector3 GetEndPosition()
    {
        return endPosition;
    }

    public bool IsFinished()
    {
        return position >= 1.0;
    }

    private double GetXPosition()
    {
        return position * distance.x;
    }

    private double GetYPosition()
    {
        double yOffset = distance.y * position;
        double yPosition = GetSquareRoot();
        if (double.IsNaN(yPosition)) yPosition = 0;
        return yPosition + yOffset;
    }

    private double GetZPosition()
    {
        return position * distance.z;
    }

    private double GetSquareRoot()
    {
        double fraction = Squared(position - width) / Squared(width);
        double toBeSquareRooted = (1 - fraction) * Squared(height);
        return Mathf.Sqrt((float)toBeSquareRooted);
    }

    private double Squared(double value)
    {
        return Mathf.Pow((float)value, 2);
    }
}
