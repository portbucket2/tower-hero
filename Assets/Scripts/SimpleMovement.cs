﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class SimpleMovement : MonoBehaviour
{

    [SerializeField] Vector3 mouseStart;
    [SerializeField] Vector3 direction;
    [SerializeField] Transform camPivot;
    [SerializeField] Transform cam;
    [SerializeField] VariableJoystick joystick;

    float horizontalInput;
    float verticalInput;

    NavMeshAgent agent;
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            mouseStart = Input.mousePosition;
        }
        if (Input.GetMouseButton(0))
        {
            horizontalInput = joystick.Horizontal;
            verticalInput = joystick.Vertical;

            direction = Input.mousePosition - mouseStart;
            direction = new Vector3(direction.x, 0f, direction.y);
            Debug.DrawLine(transform.position, direction * 2f, Color.red);        

            Vector3 camF = cam.forward;
            Vector3 camR = cam.right;

            camF.y = 0;
            camR.y = 0;
            camF = camF.normalized;
            camR = camR.normalized;

            Vector3 dd = (camF * verticalInput + camR * horizontalInput) * Time.deltaTime * 5f;
            Debug.DrawLine(transform.position, dd * 3f, Color.blue);
            transform.position += dd;
        }

        if (Input.GetMouseButtonUp(0))
        {

        }

    }
}
