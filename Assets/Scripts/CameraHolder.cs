﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraHolder : MonoBehaviour
{
    [SerializeField] bool isFollwoing = true;
    [SerializeField] Transform playerBody;
    [SerializeField] Transform centerFlag;
    [SerializeField] Transform pizza;
    [SerializeField] PlayerController player;
    [SerializeField] bool isChangable = true;
    [SerializeField] bool isPizzaCam = false;

    [Header("For Debug: ")]
    [SerializeField] float PlayerPosInangle = 0f;
    [SerializeField] float cameraSpeed = 1f;
    float angle = 0f;

    Vector3 v1 = new Vector3(0f, 22.5f, 0f);
    Vector3 v2 = new Vector3(0f, 22.5f + 45f, 0f);
    Vector3 v3 = new Vector3(0f, 22.5f + 90f, 0f);
    Vector3 v4 = new Vector3(0f, 22.5f + 135f, 0f);

    float multiplyer = 1f;

    
    RotationState rotationState;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void CameraPositionCheck()
    {
        if (isFollwoing)
        {
            //float dist = Vector3.Distance(playerBody.position, Vector3.zero);
            //Debug.Log("distance: " + dist);
            //if (dist > 3f)
            //    isChangable = true;
            //else
            //    isChangable = false;

            if (isChangable)
            {
                if (playerBody.position.x > 0)
                {
                    multiplyer = 1f;
                }
                else
                {
                    multiplyer = -1f;
                }
                angle = Vector3.Angle(playerBody.position - centerFlag.position, Vector3.forward);
                PlayerPosInangle = angle;
                //Debug.Log("Angle: " + angle);

                if (isPizzaCam)
                {
                    if (angle >= 0 && angle < 45)
                    {
                        RotationV1();
                    }
                    else if (angle >= 45 && angle < 90)
                    {
                        RotationV2();
                    }
                    else if (angle >= 90 && angle < 135)
                    {
                        RotationV3();
                    }
                    else if (angle >= 135 && angle < 180)
                    {
                        RotationV4();
                    }
                }
                else
                {
                    Vector3 rot = new Vector3(0f, angle * multiplyer, 0f);
                    transform.eulerAngles = rot;
                    pizza.eulerAngles = rot;
                }
            }
        }
    }
    void RotationV1()
    {
        //if (rotationState != RotationState.V1)
        //{
            //NotFollow();
            rotationState = RotationState.V1;
            transform.DOLocalRotate(v1 * multiplyer, cameraSpeed).SetEase(Ease.InSine).OnComplete(Follow);
            //transform.eulerAngles = v1 * multiplyer;
            pizza.eulerAngles = v1 * multiplyer;
        //}
    }
    void RotationV2()
    {
        //if (rotationState != RotationState.V2)
        //{
            //NotFollow();
            rotationState = RotationState.V2;
            transform.DOLocalRotate(v2 * multiplyer, cameraSpeed).SetEase(Ease.InSine).OnComplete(Follow);
            //transform.eulerAngles = v2 * multiplyer;
            pizza.eulerAngles = v2 * multiplyer;
        //}
    }
    void RotationV3()
    {
        //if (rotationState != RotationState.V3)
        //{
            //NotFollow();
            rotationState = RotationState.V3;
            transform.DOLocalRotate(v3 * multiplyer, cameraSpeed).SetEase(Ease.InSine).OnComplete(Follow);
            //transform.eulerAngles = v3 * multiplyer;
            pizza.eulerAngles = v3 * multiplyer;
        //}
    }
    void RotationV4()
    {
        //if (rotationState != RotationState.V4)
        //{
            //NotFollow();
            rotationState = RotationState.V4;
            transform.DOLocalRotate(v4 * multiplyer, cameraSpeed).SetEase(Ease.InSine).OnComplete(Follow);
            //transform.eulerAngles = v4 * multiplyer;
            pizza.eulerAngles = v4 * multiplyer;
        //}
    }


    void Follow()
    {

    }
    public void StartFollow()
    {
        isFollwoing = true;

    }
    public void NotFollow()
    {
        isFollwoing = false;
        //player.DisableInput();
    }

}
public enum RotationState
{
    V1,
    V2,
    V3,
    V4
}
