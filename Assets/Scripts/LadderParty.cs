﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;

public class LadderParty : MonoBehaviour
{
    [SerializeField] Transform endPoint;
    [SerializeField] Animator ladderHolder;
    [SerializeField] PlayerDetector playerDetector;
    [SerializeField] List<Rigidbody> ladderRB;
    [SerializeField] List<Transform> path;
    [SerializeField] List<Transform> partyMembers;

    [Header("For debug :: ")]
    [SerializeField] GameController gameController;
    [SerializeField] PlayerController playerController;
    [SerializeField] Vector3 targetPos;
    [SerializeField] Vector3 towerPos;
    [SerializeField] List<Animator> partyMembersAnimator;
    [SerializeField] Vector3[] ladderPath;
    [SerializeField] List<Rigidbody> ragDollRbs;

    [Header("Ram Health for debug :: ")]
    [SerializeField] int currentHealth = 10;
    [SerializeField] int maxHealth = 10;

    NavMeshAgent agent;
    bool startedShooting = false;
    Vector3 lookAtPosition;
    bool shopSetupComplete = false;

    readonly string OPEN = "open";
    readonly string RUN = "run";
    readonly string CLIMB = "climb";

    WaitForSeconds WAITFOUR = new WaitForSeconds(4f);
    WaitForSeconds WAITTWO = new WaitForSeconds(2f);
    WaitForEndOfFrame END = new WaitForEndOfFrame();

    void Start()
    {
        gameController = GameController.GetController();
        playerController = gameController.GetPlayer();
        agent = GetComponent<NavMeshAgent>();
        towerPos = new Vector3(gameController.GetTower().position.x, transform.position.y, gameController.GetTower().position.z);
        AnimatorsInit();
        Initialize(true);
    }
    private void Update()
    {
        if (!shopSetupComplete && agent.enabled)
        {
            float dist = agent.remainingDistance;
            if (dist != Mathf.Infinity && dist < 0.5f)
            {
                //Debug.Log("Enemy reached!!");
                if (!startedShooting)
                {
                    startedShooting = true;
                    // start shooting
                    StartShooting();
                }
            }
        }
        //transform.DOLookAt(lookAtPosition, 0.2f).SetEase(Ease.Flash);
    }

    public void Initialize(bool _isActiveShooter)
    {
        StopAllCoroutines();
        startedShooting = false;
        Vector3 targetPos = FindPoint();
        agent.SetDestination(targetPos);
        //lookAtPosition = targetPos;
        //Debug.DrawRay(transform.position, targetDir, Color.green, 10f);
        //GetPointDistanceFromObject(5f);
    }

    void StartShooting()
    {
        StopAllCoroutines();
        StartCoroutine(ShootingRoutine());
    }
    IEnumerator ShootingRoutine()
    {
        shopSetupComplete = true;
        if (agent.enabled)
        {
            agent.SetDestination(transform.position);
            agent.enabled = false;
        }
        SetUpLadder();
        yield return WAITFOUR;
        //StartShooting();
        //shoot
    }
    void SetUpLadder()
    {
        //startedShooting = false;
        //playerController.SendWarning();
        lookAtPosition = new Vector3(towerPos.x, transform.position.y, towerPos.z);
        transform.DOLookAt(lookAtPosition, 1f).SetEase(Ease.Flash).OnComplete(() => ladderHolder.SetTrigger(OPEN)); FindAngle(); StartCoroutine(LadderClimbingRoutine());
        
        
        // ram mechanics here
        // do damage to tower
    }
    public void TakeDamege()
    {
        //Destroy(gameObject,1f);
        if (isAlive())
            currentHealth--;
        else
            Die();
    }

    bool isAlive()
    {
        bool isAlive = true;
        if (currentHealth <= 0)
            isAlive = false;
        else
            isAlive = true;

        return isAlive;
    }
    public void Die()
    {
        Debug.Log("Ladder Dead!!");
        gameController.GetSearchCollider().RemoveDead(transform.GetComponent<Collider>());
        GetComponent<BoxCollider>().enabled = false;
        LadderKicked();
    }
    void AnimatorsInit()
    {
        partyMembersAnimator = new List<Animator>();
        int max = partyMembers.Count;
        for (int i = 0; i < max; i++)
        {
            partyMembersAnimator.Add(partyMembers[i].GetComponent<Animator>());
            GetAllComponentsInChildren(partyMembers[i]);
            RagDollKinematicTrue();
        }        
    }
    IEnumerator LadderClimbingRoutine()
    {
        int max = partyMembers.Count;
        yield return WAITTWO;
        playerDetector.StartDetecting();
        for (int i = 0; i < max; i++)
        {
            ClimbLadder(partyMembers[i]);
            yield return WAITFOUR;
        }
        
        
    }
    Vector3 FindPoint()
    {
        Vector3 direction = transform.position - towerPos;
        Vector3 point = direction.normalized * 17f;
        point = new Vector3(point.x, transform.position.y, point.z);
        Debug.DrawLine(transform.position, point, Color.green, 10f);
        return point;
    }
    void ClimbLadder( Transform climber)
    {
        int maxx = path.Count;
        ladderPath = new Vector3[maxx];        
        for (int i = 0; i < maxx; i++)
        {
            ladderPath[i] = path[i].transform.position;
        }
        //ladderPath[maxx] = gameController.transform.position;
        climber.GetComponent<Animator>().SetTrigger(CLIMB);
        climber.DOPath(ladderPath, 20f,PathType.Linear).OnComplete(() => LadderClimbComplete(climber) );
    }
    void LadderClimbComplete(Transform _climber)
    {
        int max = path.Count;
        ladderPath = new Vector3[2];
        ladderPath[0] = new Vector3(path[max - 1].transform.position.x, gameController.transform.position.y, path[max - 1].transform.position.z);
        ladderPath[1] = gameController.transform.position;

        _climber.GetComponent<Animator>().SetTrigger(RUN);
        _climber.DOPath(ladderPath, 7f, PathType.Linear).OnComplete(() => ClimbComplete(_climber)); 
    }

    void ClimbComplete(Transform climber)
    {
        climber.gameObject.SetActive(false);
        climber.DOKill();
    }
    public void LadderKicked()
    {
        // enable ladder gravity
        // eneble all character gravity
        // force ladder outside
        // play some particle
        int members = partyMembers.Count;
        for (int i = 0; i < members; i++)
        {
            GetAllComponentsInChildren(partyMembers[i]);
            RagDollKinematicFalse();
        }


        StopAllCoroutines();
        gameController.AddKill();
        agent.enabled = false;
        ladderHolder.enabled = false;
        Vector3 forceDirection = transform.position - playerController.transform.position;

        int count = ladderRB.Count;
        for (int i = 0; i < count; i++)
        {
            ladderRB[i].isKinematic = false;
            //ladderRB[i].AddForce(forceDirection * 500f);
        }
        //for (int i = 0; i < count; i++)
        //{
        //    ladderRB[i].transform.DOScale(Vector3.zero, 5f).SetEase(Ease.InSine);
        //}

        int max = partyMembersAnimator.Count;
        for (int i = 0; i < max; i++)
        {
            partyMembersAnimator[i].enabled = false;
            partyMembersAnimator[i].transform.DOKill();
        }
        //for (int i = 0; i < max; i++)
        //{
        //    partyMembersAnimator[i].transform.DOScale(Vector3.zero, 5f).SetEase(Ease.InSine);
        //}

        Destroy(gameObject, 5f);
    }
    void FindAngle()
    {
        //Vector3 dir = ladderHolder.transform.position - endPoint.transform.position;
        //float angle = Vector3.Angle(dir, Vector3.forward);
        //Debug.Log("Ladder angle: " + angle);
        ////float angle =

        //Ray ray = new Ray(endPoint.position, transform.forward);
        //RaycastHit hit;
        //Debug.DrawRay(ray.origin, ray.direction, Color.magenta, 20f);
        //if (Physics.Raycast(ray,out hit, 100))
        //{
        //    Debug.DrawLine(ray.origin, hit.point, Color.green, 20f);
        //}
    }
    void GetAllComponentsInChildren(Transform trans)
    {
        foreach (Transform child in trans)
        {
            if (child.GetComponent<Rigidbody>())
            {
                ragDollRbs.Add(child.GetComponent<Rigidbody>());
            }
            if (child.childCount > 0)
            {
                GetAllComponentsInChildren(child);
            }
            else
            {
                break;
            }
        }
    }
    void RagDollKinematicTrue()
    {
        int maxRag = ragDollRbs.Count;
        for (int i = 0; i < maxRag; i++)
        {
            ragDollRbs[i].isKinematic = true;
        }
    }
    void RagDollKinematicFalse()
    {
        int maxRag = ragDollRbs.Count;
        for (int i = 0; i < maxRag; i++)
        {
            ragDollRbs[i].isKinematic = false;
        }
    }
}
