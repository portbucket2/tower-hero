﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SearchCollider : MonoBehaviour
{
    [SerializeField] private List<Collider> colliders = new List<Collider>();

    readonly string ENEMY = "Enemy";
    readonly string RAM = "Ram";
    readonly string LADDER = "Ladder";
    readonly string CATAPULT = "Catapult";

    void Start()
    {
        
    }
    
    public List<Collider> GetColliders() { return colliders; }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag(ENEMY) || other.transform.CompareTag(RAM) || other.transform.CompareTag(LADDER) || other.transform.CompareTag(CATAPULT))
        {
            if (!colliders.Contains(other))
            {
                colliders.Add(other);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        colliders.Remove(other);
    }

    public void RemoveDead(Collider dead)
    {
        colliders.Remove(dead);
    }
}
