﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;

public class PlayerController : MonoBehaviour
{
    [SerializeField] bool inputEnabled = true;
    [SerializeField] VariableJoystick joystick;
    [SerializeField] CameraHolder cameraHolder;
    [SerializeField] Transform cam;
    [SerializeField] Transform pizzaEnd;
    [SerializeField] Transform playerBody;
    [SerializeField] float playerSpeed = 0.01f;
    [SerializeField] SearchCollider searchCollider;
    [SerializeField] GameObject arrowPrefab;
    [SerializeField] Animator animator;
    [SerializeField] ParticleSystem warningParticle;
    [SerializeField] ParticleSystem kickParticle;
    [SerializeField] Transform muzzle;
    [SerializeField] Transform shootingPosition;
    [Header("PlayerHealth ")]
    [SerializeField] int currentHealth = 100;
    [SerializeField] int maxHealth = 100;

    [Header("For Debug ")]
    [SerializeField] List<Collider> detectedEnemy;
    [SerializeField] float arrowSpeed = 1f;
    [SerializeField] float arrowHeight = 1f;
    [SerializeField] bool isMoving = false;
    [SerializeField] float shootingDelay = 0.3f;
    [SerializeField] float shootingTimer = 0f;


    Transform playerTransform;
    Rigidbody rb;

    [Header("camera Changed ")]
    [SerializeField] bool cameraChanged = false;
    [SerializeField] Vector3 mouseStart;
    [SerializeField] Vector3 direction;

    Vector3 left = new Vector3(0f, 90f, 0f);
    Vector3 right = new Vector3(0f, -90f, 0f);

    readonly string IDLE = "idle";
    readonly string RUN = "run";
    readonly string SHOOT = "shoot";
    readonly string KICK = "kick";
    readonly string SPEED = "speed";

    readonly string BULLET = "bullet";
    WaitForSeconds WAITHALF = new WaitForSeconds(0.5f);

    float horizontalInput;
    float verticalInput;
    NavMeshAgent agent;
    float multiplier = 1f;
    GameController gameController;
    AnalyticsController analyticsController;
     
    void Start()
    {
        gameController = GameController.GetController();
        analyticsController = AnalyticsController.GetController();
        agent = GetComponent<NavMeshAgent>();
        playerTransform = animator.transform;
        rb = transform.GetComponent<Rigidbody>();

        analyticsController.LevelStarted();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            inputEnabled = true;
            cameraChanged = false;
            cameraHolder.StartFollow();
            mouseStart = Input.mousePosition;
            animator.SetTrigger(RUN);
            isMoving = true;
            shootingTimer = 0;
        }
        if (inputEnabled && Input.GetMouseButton(0) )
        {
            horizontalInput = joystick.Horizontal;
            verticalInput = joystick.Vertical;

            direction = Input.mousePosition - mouseStart;
            direction = new Vector3(direction.x, 0f, direction.y);
            //Debug.DrawLine(transform.position, direction * 2f, Color.red);

            Vector3 camF = cam.forward;
            Vector3 camR = cam.right;

            camF.y = 0;
            camR.y = 0;
            camF = camF.normalized;
            camR = camR.normalized;

            Vector3 dd = (camF * verticalInput + camR * horizontalInput) * Time.deltaTime * 5f;
            //Debug.DrawLine(transform.position, dd * 3f, Color.blue);
            transform.position += dd;
            playerBody.DOLookAt(pizzaEnd.position, 0.1f);
            if (Input.mousePosition.x > mouseStart.x)
            {
                playerTransform.DOLocalRotate(right, 0.1f);
            }
            else
            {
                playerTransform.DOLocalRotate(left, 0.1f);
            }

            animator.SetFloat(SPEED, Mathf.Abs(horizontalInput));

            cameraHolder.CameraPositionCheck();
        }

        if (Input.GetMouseButtonUp(0))
        {
            playerTransform.DOLookAt(pizzaEnd.position, 0.1f);
            DirectionCheck();
            //SearchAndDestroy();
            isMoving = false;
        }

        if (!isMoving)
        {
            // shoot after interval
            if (shootingTimer > shootingDelay)
            {
                SearchAndDestroy();
                shootingTimer = 0;
            }
            else
            {
                shootingTimer += Time.deltaTime;
            }
        }
    }
    void DirectionCheck()
    {
        if (transform.position.z > 0)
        {
            multiplier = -1f;
        }
        else
        {
            multiplier = 1f;
        }
    }
    public void DisableInput()
    {
        if (!cameraChanged)
        {
            inputEnabled = false;
            cameraChanged = true;
        }        
    }
    public void SearchAndDestroy()
    {
        Vector3 spwanPosition = shootingPosition.position; //transform.TransformPoint(shootingPosition.position);
        detectedEnemy = new List<Collider>();
        detectedEnemy = searchCollider.GetColliders();
        int max = detectedEnemy.Count;

        

        // shoot all in range
        //for (int i = 0; i < max; i++)
        //{
        //    if (detectedEnemy[i] != null && detectedEnemy[i].gameObject.activeInHierarchy)
        //    {
        //        GameObject gg = Instantiate(arrowPrefab, spwanPosition, playerBody.rotation);
        //        Arrow aa = gg.GetComponent<Arrow>();
        //        aa.SetArrow(detectedEnemy[i].transform.position, arrowSpeed, arrowHeight);
        //        aa.ShootArrow();
        //    }
        //}

        // shoot only one
        if (max > 0)
        {
            animator.SetTrigger(SHOOT);
            GameObject gg = Instantiate(arrowPrefab, spwanPosition, playerBody.rotation);
            Arrow aa = gg.GetComponent<Arrow>();
            aa.SetArrow(detectedEnemy[0].transform.position, arrowSpeed, arrowHeight);
            aa.ShootArrow();
        }
    }
    public void SendWarning()
    {
        warningParticle.Play();
    }
    public void InitiateKick(Transform target)
    {
        transform.LookAt(target);
        animator.SetTrigger(KICK);
        kickParticle.transform.position = target.position;
        kickParticle.Play();
    }
    public void TakeDamege()
    {
        if (isAlive())
            currentHealth--;
        else
            Die();
    }

    bool isAlive()
    {
        bool isAlive = true;
        if (currentHealth <= 0)
            isAlive = false;
        else
            isAlive = true;

        return isAlive;
    }
    void Die()
    {
        Debug.Log("Player Dead");
        agent.enabled = false;
        animator.enabled = false;
        DisableInput();

        gameController.PlayerDead();
    }
}
