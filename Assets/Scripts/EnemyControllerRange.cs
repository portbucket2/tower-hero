﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;

public class EnemyControllerRange : MonoBehaviour
{
    /*Enemy range Behavior
     * Spwan away from player
     * get a random distance close to the castle
     * start shooting arrow/spear if active shooter
     * die when hit with player arrow or cannon ball
    */

    [SerializeField] bool isArcher = true;
    [SerializeField] GameObject arrowPrefab;
    [SerializeField] GameObject spearPrefab;
    [SerializeField] Transform ShootingPosition;
    [SerializeField] Animator animator;
    [SerializeField] GameObject bow;
    [SerializeField] GameObject spear;
    [SerializeField] ParticleSystem getHitParticle;
    [SerializeField] SkinnedMeshRenderer renderer;
    [SerializeField] Material greyMaterial;

    [Header("For debug : ")]
    [SerializeField] bool isActiveShooter = true;
    [SerializeField] GameController gameController;
    [SerializeField] PlayerController playerController;
    [SerializeField] Vector3 targetPos;
    [SerializeField] Vector3 towerPos;
    [SerializeField] float arrowSpeed = 1f;
    [SerializeField] float arrowHeight = 5f;
    [SerializeField] List<Rigidbody> ragDollRbs;

    readonly string IDLE = "idle";
    readonly string RUN = "run";
    readonly string SHOOT = "shoot";
    readonly string THROW = "throw";
    readonly string CLIMB = "climb";


    NavMeshAgent agent;
    bool startedShooting = false;
    Vector3 lookAtPosition;
    WaitForSeconds WAIT = new WaitForSeconds(5f);
    WaitForSeconds WAITHALF = new WaitForSeconds(0.3f);
    WaitForEndOfFrame END = new WaitForEndOfFrame();

    void Start()
    {
        gameController = GameController.GetController();
        playerController = gameController.GetPlayer();
        agent = GetComponent<NavMeshAgent>();
        towerPos = new Vector3(gameController.GetTower().position.x, transform.position.y, gameController.GetTower().position.z);
        Initialize(false);
        GetAllComponentsInChildren(animator.transform);
        RagDollKinematicTrue();
        if (isArcher)
        {
            bow.SetActive(true);
            spear.SetActive(false);
        }
        else
        {
            bow.SetActive(false);
            spear.SetActive(true);
        }
            
    }
    private void Update()
    {
        if (!startedShooting && agent.enabled)
        {
            float dist = agent.remainingDistance;
            if (dist != Mathf.Infinity && agent.pathStatus == NavMeshPathStatus.PathComplete && agent.remainingDistance == 0)
            {
                //Debug.Log("Enemy reached!!");
                startedShooting = true;
                // start shooting
                StartShooting();
            }
            //transform.DOLookAt(lookAtPosition, 0.2f).SetEase(Ease.Flash);
        }
    }
    public void Initialize(bool firstEncounterDone)
    {
        StopAllCoroutines();
        startedShooting = false;
        Vector2 temp;
        Vector3 targetPos;
        if (firstEncounterDone)
        {
            temp = Random.insideUnitCircle.normalized * Random.Range(1f, 5f);// RandomPointInAnnulus(Vector2.zero, 10f, 25f);
            targetPos = new Vector3(temp.x + transform.position.x, 0, temp.y + transform.position.z); //+ tower.position;
            targetPos.y = -12.8f;            
        }
        else
        {
            //temp = Random.insideUnitCircle.normalized * Random.Range(18f, 23f);
            //targetPos =  new Vector3(temp.x, 0, temp.y); //+ tower.position;
            //targetPos.y = -12.8f;
            targetPos = FindPoint();
        }
        

        Debug.DrawLine(transform.position, targetPos, Color.red, 7f);
        agent.SetDestination(targetPos);
        animator.SetTrigger(RUN);
        lookAtPosition = targetPos;
        //Debug.DrawRay(transform.position, targetDir, Color.green, 10f);
        //GetPointDistanceFromObject(5f);
    }
    public void SetType(bool _isArchar)
    {
        isArcher = _isArchar;
        if (isArcher)
        {
            bow.SetActive(true);
            spear.SetActive(false);
        }
        else
        {
            bow.SetActive(false);
            spear.SetActive(true);
        }
    }
    void StartShooting()
    {
        //Debug.Log("startedShooting");
        StartCoroutine(ShootingRoutine());
    }
    IEnumerator ShootingRoutine()
    {
        playerController.SendWarning();
        lookAtPosition = new Vector3(playerController.transform.position.x, transform.position.y, playerController.transform.position.z);
        if (isArcher)
            animator.SetTrigger(SHOOT);
        else
            animator.SetTrigger(THROW);
        transform.DOLookAt(lookAtPosition, 0.2f).SetEase(Ease.Flash);

        yield return WAITHALF;
        Shoot();
        yield return WAIT;
        Initialize(true);
        //shoot
    }
    void Shoot()
    {
        GameObject gg;

        if (isArcher)
            gg = Instantiate(arrowPrefab, ShootingPosition.position, arrowPrefab.transform.rotation);
        else
            gg = Instantiate(spearPrefab, ShootingPosition.position, arrowPrefab.transform.rotation);

        Arrow aa = gg.GetComponent<Arrow>();
        aa.SetArrow(playerController.transform.position, arrowSpeed, arrowHeight);
        aa.ShootArrow();
    }
    public void TakeDamege()
    {
        getHitParticle.Play();
        gameController.GetSearchCollider().RemoveDead(transform.GetComponent<Collider>());
        Die();        
    }
    void Die()
    {
        renderer.material = greyMaterial;
        RagDollKinematicFalse();
        StopAllCoroutines();
        agent.enabled = false;
        gameController.AddKill();
        animator.enabled = false;
        Destroy(gameObject, 5f);
        transform.GetComponent<CapsuleCollider>().enabled = false;
    }
    Vector3 FindPoint()
    {
        Vector3 direction = transform.position - towerPos;
        Vector3 point = direction.normalized * Random.Range(15f, 20f);
        float xOffset = Random.Range(4f, 8f);
        point = new Vector3(point.x + xOffset, transform.position.y, point.z);
        Debug.DrawLine(transform.position, point, Color.green, 10f);
        return point;
    }


    void GetAllComponentsInChildren(Transform trans)
    {
        foreach (Transform child in trans)
        {
            if (child.GetComponent<Rigidbody>())
            {
                ragDollRbs.Add(child.GetComponent<Rigidbody>());
            }
            if (child.childCount > 0)
            {
                GetAllComponentsInChildren(child);
            }
            else
            {
                break;
            }
        }
    }
    void RagDollKinematicTrue()
    {
        int maxRag = ragDollRbs.Count;
        for (int i = 0; i < maxRag; i++)
        {
            ragDollRbs[i].isKinematic = true;
        }
    }
    void RagDollKinematicFalse()
    {
        int maxRag = ragDollRbs.Count;
        for (int i = 0; i < maxRag; i++)
        {
            ragDollRbs[i].isKinematic = false;
        }
    }



}
