﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController gameController;

    public int killCount = 0;
    public int TotalEnemies = 20;

    private void Awake()
    {
        gameController = this;    
    }

    public static GameController GetController()
    {
        return gameController;
    }

    [SerializeField] PlayerController player;
    [SerializeField] SearchCollider searchCollider;
    [SerializeField] Transform tower;
    [SerializeField] UIController uiController;
    [SerializeField] CameraHolder cameraHolder;
    [SerializeField] Camera mainCamera;
    

    void Start()
    {
        UpdateKillCount(0,TotalEnemies);
    }

    public PlayerController GetPlayer() { return player; }
    public SearchCollider GetSearchCollider() { return searchCollider; }
    public Transform GetTower() { return tower; }
    public Camera GetCamera() { return mainCamera; }
    public UIController GetUIController() { return uiController; }
    public void AddKill() { killCount++; LevelEndCheck(); }
    public void UpdateKillCount(int current, int total)
    {
        uiController.UpdateKillCount(current, total);
    }
    public void SetTotalEnemies(int total) {
        TotalEnemies = total;
        UpdateKillCount(killCount, TotalEnemies);
    }
    public void PlayerDead()
    {
        uiController.ResetLevel();
    }

    void LevelEndCheck()
    {
        UpdateKillCount(killCount, TotalEnemies);
        if (killCount >= TotalEnemies)
        {
            Debug.Log("level ended");
            uiController.LevelEndPanel();
        }
    }
}
