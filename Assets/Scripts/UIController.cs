﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class UIController : MonoBehaviour
{
    [SerializeField] Button testButton;
    [SerializeField] Button nextLevelButton;
    [SerializeField] GameObject levelEndPanel;
    [SerializeField] Transform tower;
    [SerializeField] Image stayCountImage;
    [SerializeField] GameObject tutorialPanel;
    [SerializeField] Button tutorialPanelButton;
    [SerializeField] Image progressbarImage;
    [SerializeField] GameObject levelResetPanel;
    [SerializeField] Button levelResetButton;

    Vector3 offset = new Vector3(0f,100f,0f);

    GameController gameController;
    PlayerController playerController;
    GameManager gameManager;
    AnalyticsController analyticsController;

    void Start()
    {
        gameManager = GameManager.GetManager();
        analyticsController = AnalyticsController.GetController();
        float height = Screen.height * 0.10f;
        offset = new Vector3(0f, height, 0f);
        gameController = GameController.GetController();
        playerController = gameController.GetPlayer();

        testButton.onClick.AddListener(delegate {
            SceneManager.LoadScene(0);           
        });
        nextLevelButton.onClick.AddListener(delegate {
            gameManager.GotoNextStage();
            analyticsController.LevelEnded();
        });
        tutorialPanelButton.onClick.AddListener(delegate {
            Time.timeScale = 1f;
            tutorialPanel.SetActive(false);
            gameManager.TutorialSeen();
        });
        levelResetButton.onClick.AddListener(delegate {
            gameManager.ResetStage();
            analyticsController.LevelFailed();
        });

        if (!gameManager.TutorialAlreadySeen())
        {
            ShowTutorial();
        }
    }

    public void LevelEndPanel()
    {
        levelEndPanel.SetActive(true);
    }
    public void UpdateKillCount(int current, int total)
    {
        float print = (float)current / total;
        if (float.IsNaN(print))
        {
            print = 0;
        }
        progressbarImage.fillAmount = print;
    }
    public void StayTimeCounter(float current, float max)
    {
        float temp = (float)current / max;
        stayCountImage.fillAmount = temp;
    }
    public void StayCountImageShowOn(bool show)
    {
        if (show)
        {
            stayCountImage.gameObject.SetActive(true);
            // set position
            Vector3 pos = Camera.main.WorldToScreenPoint(playerController.transform.position);
            //Debug.LogError("image position: " + pos);
            stayCountImage.rectTransform.position = pos + offset;
        }
        else
        {
            stayCountImage.gameObject.SetActive(false);
        }
    }
    public void ShowTutorial()
    {
        Time.timeScale = 0.05f;
        tutorialPanel.SetActive(true);
    }
    public void ResetLevel()
    {
        levelResetPanel.SetActive(true);
    }
}
