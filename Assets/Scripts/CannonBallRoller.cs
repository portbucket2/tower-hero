﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CannonBallRoller : MonoBehaviour
{
    [SerializeField] bool isKickable = false;
    [SerializeField] ParticleSystem explotion;
    [SerializeField] bool isPlayerClose = false;
    [SerializeField] float stayTime = 3f;
    [SerializeField] float damageRadius = 5f;

    float count = 0;

    Rigidbody rb;

    readonly string ENEMY = "Enemy";
    readonly string RAM = "Ram";
    readonly string LADDER = "Ladder";
    readonly string CATAPULT = "Catapult";
    readonly string PLAYER = "Player";
    readonly string GROUND = "Ground";
    WaitForSeconds TIME = new WaitForSeconds(9f);
    WaitForSeconds WAIT = new WaitForSeconds(1f);
    WaitForSeconds WAITHALF = new WaitForSeconds(0.5f);

    GameController gameController;
    PlayerController playerController;
    UIController uiController;

    void Start()
    {
        gameController = GameController.GetController();
        playerController = gameController.GetPlayer();
        uiController = gameController.GetUIController();
        rb = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (isKickable)
        {
            if (collision.transform.CompareTag(GROUND))
            {
                Debug.Log("cannon hit ground");
                Explode();
            }            
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag(PLAYER))
        {
            isPlayerClose = true;
            count = 0;
            uiController.StayCountImageShowOn(true);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.transform.CompareTag(PLAYER) && isPlayerClose)
        {
            if (count > stayTime)
            {
                KickBall();
            }
            else
            {
                count += Time.deltaTime;
                // update UI
                uiController.StayTimeCounter(count, stayTime);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.transform.CompareTag(PLAYER))
        {
            isPlayerClose = false;
            uiController.StayCountImageShowOn(false);
        }
    }
    void KickBall()
    {
        Debug.Log("player kick ball");
        // countdown timer start
        StartCoroutine(LifeEndRoutine());
    }

    IEnumerator LifeEndRoutine()
    {
        
        transform.GetComponent<Collider>().isTrigger = false;
        playerController.InitiateKick(transform);
        Vector3 dir = transform.position - gameController.transform.position;
        Vector3 target = transform.position + dir * 0.5f;
        Debug.DrawLine(transform.position, target,Color.red, 10f);
        //Vector3 force = (dir.normalized * 3f) + transform.up * 5f;
        transform.DOMove(target, 0.2f).SetEase(Ease.Flash).OnComplete(EnablePhysics);
        
        //yield return WAITHALF;
        
        yield return TIME;
        //Explode();
        //yield return WAIT;
        //gameObject.SetActive(false);
    }
    void EnablePhysics()
    {
        rb.isKinematic = false;
        rb.AddForce(-transform.up * 5f, ForceMode.Impulse);
        transform.GetComponent<Collider>().isTrigger = false;
    }
    public void ActivateCannonBall()
    {
        isKickable = true;        
    }
    void Explode()
    {
        explotion.transform.localPosition = transform.localPosition;
        explotion.Play();

        Collider[] hitColliders = Physics.OverlapSphere(transform.position, damageRadius);
        foreach (var hitCollider in hitColliders)
        {
            //hitCollider.SendMessage("AddDamage");
            CheckAreaForDamage(hitCollider);
        }
        gameObject.SetActive(false);
    }
    void CheckAreaForDamage(Collider collision)
    {
        if (collision.transform.CompareTag(ENEMY))
        {
            Debug.Log("cannonball hit enemy");
            collision.transform.GetComponent<EnemyControllerRange>().TakeDamege();
        }
        else if (collision.transform.CompareTag(RAM))
        {
            Debug.Log("cannonball hit ram");
            collision.transform.GetComponent<BattaringRam>().TakeDamege();
        }
        else if (collision.transform.CompareTag(LADDER))
        {
            Debug.Log("cannonball hit ladder");
            collision.transform.GetComponent<LadderParty>().TakeDamege();
        }
        else if (collision.transform.CompareTag(CATAPULT))
        {
            Debug.Log("Arrow hit catapult");
            collision.transform.GetComponent<Catapult>().TakeDamege();
        }
    }
}
