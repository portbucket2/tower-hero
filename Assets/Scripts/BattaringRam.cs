﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;

public class BattaringRam : MonoBehaviour
{

    [SerializeField] Animator ramAnimator;
    [SerializeField] Animator wheelsAnimator;
    [SerializeField] ParticleSystem destroyedParticle;
    [SerializeField] List<Rigidbody> breakables;

    [Header("For debug :: ")]
    [SerializeField] GameController gameController;
    [SerializeField] PlayerController playerController;
    [SerializeField] Vector3 targetPos;
    [SerializeField] Vector3 towerPos;

    [Header("Ram Health for debug :: ")]
    [SerializeField] int currentHealth = 10;
    [SerializeField] int maxHealth = 10;

    NavMeshAgent agent;
    bool startedShooting = false;
    Vector3 lookAtPosition;

    bool shopSetupComplete = false;

    readonly string RAM = "ram";
    readonly string DEFAULT = "default";
    readonly string MOVE = "move";

    WaitForSeconds WAIT = new WaitForSeconds(4f);
    WaitForEndOfFrame END = new WaitForEndOfFrame();

    Vector3 rammingPos = new Vector3(0f,1.5f,2f);
    Vector3 rammingReverse = new Vector3(0f,1.5f,-2f);
    Vector3 ramNormpos = new Vector3(0f,1.5f,0f);
    void Start()
    {
        gameController = GameController.GetController();
        playerController = gameController.GetPlayer();
        agent = GetComponent<NavMeshAgent>();
        towerPos = gameController.GetTower().position;
        Initialize(true);
        //RamTower();
    }
    private void Update()
    {
        if (!shopSetupComplete && agent.enabled)
        {
            float dist = agent.remainingDistance;
            if (dist != Mathf.Infinity && dist < 0.5f)
            {
                //Debug.Log("Enemy reached!!");
                if (!startedShooting)
                {
                    startedShooting = true;
                    // start shooting
                    StartShooting();
                }
            }
        }
        //transform.DOLookAt(lookAtPosition, 0.2f).SetEase(Ease.Flash);
    }

    public void Initialize(bool _isActiveShooter)
    {
        StopAllCoroutines();
        startedShooting = false;
        Vector3 targetPos = FindPoint();
        agent.SetDestination(targetPos);
        //lookAtPosition = targetPos;

        //Debug.DrawRay(transform.position, targetDir, Color.green, 10f);
        //GetPointDistanceFromObject(5f);
    }

    void StartShooting()
    {
        StopAllCoroutines();
        StartCoroutine(ShootingRoutine());
    }
    IEnumerator ShootingRoutine()
    {
        shopSetupComplete = true;
        RamTower();
        yield return WAIT;
        StartShooting();
        //shoot
    }
    void RamTower()
    {
        if (agent.enabled)
        {
            agent.SetDestination(transform.position);
            agent.enabled = false;
        }
        wheelsAnimator.SetTrigger(DEFAULT);
        playerController.SendWarning();
        lookAtPosition = new Vector3(towerPos.x, transform.position.y, towerPos.z);
        transform.DOLookAt(lookAtPosition, 0.2f).SetEase(Ease.Flash);

        ramAnimator.SetTrigger(RAM);
        // ram mechanics here
        // do damage to tower
    }
    public void TakeDamege()
    {
        //Destroy(gameObject,1f);
        if (isAlive())
            currentHealth--;
        else
            Die();
    }

    bool isAlive()
    {
        bool isAlive = true;
        if (currentHealth <= 0)
            isAlive = false;
        else
            isAlive = true;

        return isAlive;
    }
    void Die()
    {
        //gameObject.SetActive(false);
        StopAllCoroutines();
        gameController.AddKill();
        agent.enabled = false;
        gameController.GetSearchCollider().RemoveDead(transform.GetComponent<Collider>());
        ramAnimator.enabled = false;
        wheelsAnimator.enabled = false;
        destroyedParticle.Play();
        int max = breakables.Count;
        for (int i = 0; i < max; i++)
        {
            breakables[i].isKinematic = false;
            breakables[i].AddForce(transform.up * 2f, ForceMode.Impulse);
        }
        for (int i = 0; i < max; i++)
        {
            breakables[i].transform.DOScale(Vector3.zero, 5f).SetEase(Ease.InSine);
        }


        Destroy(gameObject, 5f);
    }
    Vector3 FindPoint()
    {
        Vector3 direction = transform.position - towerPos;
        Vector3 point = direction.normalized * 15f;
        point = new Vector3(point.x, transform.position.y, point.z);
        Debug.DrawLine(transform.position, point, Color.green, 10f);
        return point;
    }
}



