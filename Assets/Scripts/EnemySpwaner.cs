﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EnemySpwaner : MonoBehaviour
{
    [Header("Values: ")]
    [SerializeField] float counter = 0f;
    [SerializeField] float spwanDelay = 10f;
    [SerializeField] int startingEnemy = 10;
    [SerializeField] Transform indicator;
    [Header("Enemy types")]
    [SerializeField] GameObject prefabEnemyRange;
    [SerializeField] GameObject batteringRam;
    [SerializeField] GameObject ladderParty;
    [SerializeField] GameObject catapult;
    [SerializeField] List<Transform> spwanPoints;

    ParticleSystem indicatorParticle;

    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    WaitForSeconds WAITFIVE = new WaitForSeconds(5f); 

    Vector3 spwanPosition;
    Vector3 offset = new Vector3(1f, 0f, 0f);
    GameController gameController;
    GameManager gameManager;
    int currentLevel = 1;
    void Start()
    {
        gameManager = GameManager.GetManager();
        gameController = GameController.GetController();
        indicatorParticle = indicator.GetChild(0).GetComponent<ParticleSystem>();
        //SpwanRangeEnemy(startingEnemy);
        if (gameManager)
        {
            currentLevel = gameManager.GetlevelCount();
        }        
        SpwanEnemyByLevel();
    }

    // Update is called once per frame
    void Update()
    {
        // continuous enemy spwan  !!
        //if (counter > spwanDelay) 
        //{
        //    SpwanRangeEnemy(2);
        //    counter = 0;
        //}
        //else
        //{
        //    counter += Time.deltaTime;
        //}
    }
    void SpwanRangeEnemy(int number, bool isRandom = false)
    {
        int max = spwanPoints.Count;
        int random = Random.Range(0, max);
        spwanPosition = spwanPoints[random].position;


        for (int i = 0; i < number; i++)
        {
            if (isRandom)
            {
                Vector2 temp = Random.insideUnitCircle.normalized * 40f;// RandomPointInAnnulus(Vector2.zero, 10f, 25f);
                spwanPosition = new Vector3(temp.x, 0, temp.y); //+ tower.position;
                spwanPosition.y = -12.8f;
            }
            else
            {
                //show indicator here
                ShowSpwanDirection();
            }
            GameObject gg = Instantiate(prefabEnemyRange, spwanPosition + offset * i, prefabEnemyRange.transform.rotation, transform);
            EnemyControllerRange ee = gg.GetComponent<EnemyControllerRange>();

            if (Random.Range(0f,1f)> 0.5f)
                ee.SetType(true);
            else
                ee.SetType(false);
        }
        
    }
    void SpwanBatteringRam()
    {
        int max = spwanPoints.Count;
        int random = Random.Range(0, max);
        spwanPosition = spwanPoints[random].position;

        ShowSpwanDirection();
        //Vector2 temp = Random.insideUnitCircle.normalized * 30f;// RandomPointInAnnulus(Vector2.zero, 10f, 25f);
        //Vector3 spwanPos = new Vector3(temp.x, 0, temp.y); //+ tower.position;
        //spwanPos.y = -12.8f;

        GameObject gg = Instantiate(batteringRam, spwanPosition, batteringRam.transform.rotation, transform);
        //EnemyControllerRange ee = gg.GetComponent<EnemyControllerRange>();
    }
    void SpwanLadderParty()
    {
        int max = spwanPoints.Count;
        int random = Random.Range(0, max);
        spwanPosition = spwanPoints[random].position;

        ShowSpwanDirection();
        //Vector2 temp = Random.insideUnitCircle.normalized * 30f;// RandomPointInAnnulus(Vector2.zero, 10f, 25f);
        //Vector3 spwanPos = new Vector3(temp.x, 0, temp.y); //+ tower.position;
        //spwanPos.y = -12.8f;

        GameObject gg = Instantiate(ladderParty, spwanPosition, ladderParty.transform.rotation, transform);
        //EnemyControllerRange ee = gg.GetComponent<EnemyControllerRange>();
    }
    void SpwanCatapult()
    {
        int max = spwanPoints.Count;
        int random = Random.Range(0, max);
        spwanPosition = spwanPoints[random].position;

        ShowSpwanDirection();
        //Vector2 temp = Random.insideUnitCircle.normalized * 30f;// RandomPointInAnnulus(Vector2.zero, 10f, 25f);
        //Vector3 spwanPos = new Vector3(temp.x, 0, temp.y); //+ tower.position;
        //spwanPos.y = -12.8f;

        GameObject gg = Instantiate(catapult, spwanPosition, catapult.transform.rotation, transform);
        //EnemyControllerRange ee = gg.GetComponent<EnemyControllerRange>();
    }
    void ShowSpwanDirection()
    {
        Vector3 dir = new Vector3(spwanPosition.x, indicator.position.y, spwanPosition.z);
        indicator.DOLookAt(dir, 0.2f).SetEase(Ease.Flash);
        //indicatorParticle.Play();
    }

    void SpwanEnemyByLevel()
    {
        switch (currentLevel)
        {
            case 1:
                gameController.SetTotalEnemies(15);
                LevelOneSpwans();
                break;
            case 2:
                gameController.SetTotalEnemies(7);
                LevelTwoSpwans();
                break;
            case 3:
                gameController.SetTotalEnemies(11);
                LevelThreeSpwans();
                break;
            case 4:
                gameController.SetTotalEnemies(11);
                LevelFourSpwans();
                break;
            case 5:
                gameController.SetTotalEnemies(29);
                LevelFiveSpwans();
                break;
            default:
                gameController.SetTotalEnemies(15);
                LevelOneSpwans();
                break;
        }
    }

    void LevelOneSpwans() { StartCoroutine(LevelOneSpwanRoutine()); }
    void LevelTwoSpwans() { StartCoroutine(LevelTwoSpwanRoutine()); }
    void LevelThreeSpwans() { StartCoroutine(LevelThreeSpwanRoutine()); }
    void LevelFourSpwans() { StartCoroutine(LevelFourSpwanRoutine()); }
    void LevelFiveSpwans() { StartCoroutine(LevelFiveSpwanRoutine()); }


    IEnumerator LevelOneSpwanRoutine() {
        SpwanRangeEnemy(5);
        yield return WAITFIVE;
        SpwanRangeEnemy(5);
        yield return WAITFIVE;
        SpwanRangeEnemy(5);
    }
    IEnumerator LevelTwoSpwanRoutine() {
        SpwanLadderParty();
        yield return WAITFIVE;
        SpwanLadderParty();
        yield return WAITFIVE;
        SpwanRangeEnemy(5);
    }
    IEnumerator LevelThreeSpwanRoutine() {
        SpwanRangeEnemy(5);
        yield return WAITFIVE;
        SpwanLadderParty();
        yield return WAITFIVE;
        SpwanRangeEnemy(5);
    }
    IEnumerator LevelFourSpwanRoutine() {
        SpwanRangeEnemy(5);
        yield return WAITFIVE;
        SpwanCatapult();
        yield return WAITFIVE;
        SpwanRangeEnemy(5);
    }
    IEnumerator LevelFiveSpwanRoutine() {
        SpwanRangeEnemy(5);
        SpwanCatapult();
        yield return WAITFIVE;
        SpwanLadderParty();
        yield return WAITFIVE;
        SpwanRangeEnemy(5);
        SpwanBatteringRam();
        yield return WAITFIVE;
        SpwanRangeEnemy(5);
        SpwanLadderParty();
        yield return WAITFIVE;
        SpwanRangeEnemy(5);
        SpwanRangeEnemy(5);
    }


}
